import 'package:get/get.dart';

import 'package:game_tracker/src/services/i18n/en_us.dart';
import 'package:game_tracker/src/services/i18n/pt_br.dart';

class Translation extends Translations {
  @override
  Map<String, Map<String, String>> get keys =>
      {'en_US': messagesEnUs, 'pt_BR': messagesPtBr};
}
