import 'package:flutter/material.dart';

import 'package:get/get.dart';

class Internacionalization {
  String translate(String value) {
    return value.tr;
  }

  void updateLocale(Locale locale) {
    Get.updateLocale(locale);
  }
}
