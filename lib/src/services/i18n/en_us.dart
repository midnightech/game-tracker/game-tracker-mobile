const Map<String, String> messagesEnUs = {
  'email': 'Email',
  'password': 'Password',
  'recovery_password': 'Recovery password',
  'sign_in': 'Sign in',
  'error_message_empty': 'Please enter some text'
};
