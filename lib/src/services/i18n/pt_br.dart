const Map<String, String> messagesPtBr = {
  'email': 'Email',
  'password': 'Senha',
  'recovery_password': 'Recuperar senha',
  'sign_in': 'Entrar',
  'error_message_empty': 'Por favor, digite algum texto'
};
