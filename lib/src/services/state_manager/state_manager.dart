import 'package:flutter/material.dart';

import 'package:get/get.dart';

class StateManager {
  /// Updates when there are changes
  Obx obs(Widget Function() builder) {
    return Obx(builder);
  }
}
