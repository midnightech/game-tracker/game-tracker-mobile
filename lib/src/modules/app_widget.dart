import 'package:flutter/material.dart';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:get/get.dart';

import 'package:game_tracker/src/modules/theme/custom_theme_data.dart';
import 'package:game_tracker/src/modules/theme/theme_management.dart';
import 'package:game_tracker/src/services/i18n/translation.dart';
import 'package:game_tracker/src/services/state_manager/state_manager.dart';

class AppWidget extends StatefulWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  State<AppWidget> createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {
  final ThemeManagement _themeManagement = Modular.get<ThemeManagement>();
  final CustomThemeData _customThemeData = Modular.get<CustomThemeData>();
  final StateManager _stateManager = Modular.get<StateManager>();

  @override
  void initState() {
    super.initState();
    _themeManagement.getThemeSystem();
  }

  @override
  Widget build(BuildContext context) {
    return _stateManager.obs(
      () => GetMaterialApp.router(
        title: 'Game Tracker',
        theme: _customThemeData.themeDataLight,
        darkTheme: _customThemeData.themeDataDark,
        themeMode:
            _themeManagement.isDark.value ? ThemeMode.dark : ThemeMode.light,
        routeInformationParser: Modular.routeInformationParser,
        routerDelegate: Modular.routerDelegate,
        translations: Translation(),
        locale: const Locale('en', 'US'),
        fallbackLocale: const Locale('pt', 'BR'),
      ),
    );
  }
}
