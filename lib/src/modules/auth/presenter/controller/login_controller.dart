import 'package:flutter/material.dart';

import 'package:get/get.dart';

class LoginController extends GetxController {
  RxBool switcherLanguage = false.obs;

  void _changeLanguage(
    void Function(Locale) updateLocaleA,
    void Function(Locale) updateLocaleB,
  ) {
    if (switcherLanguage.value) {
      updateLocaleA(const Locale('pt'));
    } else {
      updateLocaleB(const Locale('en'));
    }
  }

  void toogleSwitcherLanguage(
    void Function(Locale) updateLocaleA,
    void Function(Locale) updateLocaleB,
  ) {
    switcherLanguage.toggle();
    _changeLanguage(updateLocaleA, updateLocaleB);
  }
}
