import 'package:flutter_modular/flutter_modular.dart';

import 'package:game_tracker/src/modules/auth/presenter/controller/login_controller.dart';

import 'pages/forget_password.dart';
import 'pages/login/login.dart';

class AuthModule extends Module {
  @override
  List<Bind> get binds => [
        Bind.lazySingleton((i) => LoginController()),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/', child: (context, args) => const LoginPage()),
        ChildRoute('/forget-password',
            child: (context, args) => const ForgetPasswordPage()),
      ];
}
