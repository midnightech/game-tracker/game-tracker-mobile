import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class ForgetPasswordPage extends StatelessWidget {
  const ForgetPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Forget Password')),
      body: Center(
        child: ElevatedButton(
            onPressed: () {
              Modular.to.pop();
            },
            child: const Text('Voltar')),
      ),
    );
  }
}
