import 'package:flutter/material.dart';

import 'package:flutter_modular/flutter_modular.dart';

import 'package:game_tracker/src/modules/auth/presenter/widgets/elevated_button_custom.dart';
import 'package:game_tracker/src/modules/auth/presenter/widgets/text_form_field_custom.dart';
import 'package:game_tracker/src/modules/theme/constants.dart';
import 'package:game_tracker/src/services/i18n/main.dart';

class FormGroup extends StatefulWidget {
  const FormGroup({Key? key}) : super(key: key);

  @override
  State<FormGroup> createState() => _FormGroupState();
}

class _FormGroupState extends State<FormGroup> {
  final _formKey = GlobalKey<FormState>();
  final _internacionalization = Modular.get<Internacionalization>();

  String? _validation(String? value) {
    if (value == null || value.isEmpty) {
      return _internacionalization.translate('error_message_empty');
    }
    return null;
  }

  void _signIn() {
    if (_formKey.currentState!.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Success!'),
          duration: Duration(milliseconds: 2000),
        ),
      );
      Future.delayed(
        const Duration(milliseconds: 2500),
      ).then((_) => Modular.to.navigate('/game/'));
    }
  }

  void _forgetPassword() {
    Modular.to.pushNamed('./forget-password');
  }

  Widget _spacing() {
    return const SizedBox(
      height: 10,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormFieldCustom(
            hintText: _internacionalization.translate('email'),
            validator: _validation,
          ),
          _spacing(),
          TextFormFieldCustom(
            hintText: _internacionalization.translate('password'),
            validator: _validation,
            obscureText: true,
          ),
          _spacing(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Flexible(
                // TODO mudar a cor do Hover
                child: ElevatedButtonCustom(
                  textButton:
                      _internacionalization.translate('recovery_password'),
                  onPressed: _forgetPassword,
                  padding: const EdgeInsets.all(Constants.buttonPadding),
                ),
              ),
            ],
          ),
          _spacing(),
          SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              onPressed: _signIn,
              child: Text(
                _internacionalization.translate('sign_in'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
