import 'package:flutter/material.dart';

import 'package:flutter_modular/flutter_modular.dart';

import 'package:game_tracker/src/modules/auth/presenter/controller/login_controller.dart';
import 'package:game_tracker/src/modules/auth/presenter/pages/login/widgets/form_group.dart';
import 'package:game_tracker/src/modules/theme/constants.dart';
import 'package:game_tracker/src/modules/theme/theme_management.dart';
import 'package:game_tracker/src/services/i18n/main.dart';
import 'package:game_tracker/src/services/state_manager/state_manager.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _internacionalization = Modular.get<Internacionalization>();
  final _loginController = Modular.get<LoginController>();
  final _themeManagement = Modular.get<ThemeManagement>();
  final _stateManager = Modular.get<StateManager>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Center(
              child: Padding(
                padding: EdgeInsets.all(Constants.pagePadding),
                child: FormGroup(),
              ),
            ),
            _stateManager.obs(() => Switch(
                value: _themeManagement.isDark.value,
                onChanged: (value) => _themeManagement.toggleTheme(value))),
            Switch(
              value: _loginController.switcherLanguage.value,
              onChanged: (value) {
                _loginController.toogleSwitcherLanguage(
                  _internacionalization.updateLocale,
                  _internacionalization.updateLocale,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
