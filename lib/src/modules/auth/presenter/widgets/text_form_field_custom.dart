import 'package:flutter/material.dart';

import 'package:game_tracker/src/modules/theme/theme_text_form_field.dart';

class TextFormFieldCustom extends StatefulWidget {
  const TextFormFieldCustom({
    Key? key,
    required this.hintText,
    this.validator,
    this.obscureText,
  }) : super(key: key);

  final String hintText;
  final String? Function(String?)? validator;
  final bool? obscureText;

  @override
  State<TextFormFieldCustom> createState() => _TextFormFieldCustomState();
}

class _TextFormFieldCustomState extends State<TextFormFieldCustom> {
  @override
  Widget build(BuildContext context) {
    final ThemeTextFormField themeTextFormField =
        Theme.of(context).extension<ThemeTextFormField>()!;

    return TextFormField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        filled: true,
        border: themeTextFormField.outlineInputBorder,
        fillColor: themeTextFormField.fillColor,
        hintText: widget.hintText,
        errorStyle: themeTextFormField.errorStyle,
      ),
      validator: widget.validator,
      obscureText: widget.obscureText ?? false,
    );
  }
}
