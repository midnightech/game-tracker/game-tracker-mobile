import 'package:flutter/material.dart';

import 'package:game_tracker/src/modules/theme/theme_colors.dart';
import 'package:game_tracker/src/modules/theme/theme_elevated_button.dart';

class ElevatedButtonCustom extends StatefulWidget {
  const ElevatedButtonCustom({
    Key? key,
    required this.textButton,
    this.onPressed,
    this.backgroundColor,
    this.foregroundColor,
    this.padding,
    this.elevation,
  }) : super(key: key);

  final void Function()? onPressed;
  final String textButton;
  final Color? backgroundColor;
  final Color? foregroundColor;
  final EdgeInsets? padding;
  final double? elevation;

  @override
  State<ElevatedButtonCustom> createState() => _ElevatedButtonCustomState();
}

class _ElevatedButtonCustomState extends State<ElevatedButtonCustom> {
  @override
  Widget build(BuildContext context) {
    final ThemeElevatedButton themeElevatedButton =
        Theme.of(context).extension<ThemeElevatedButton>()!;
    final ThemeColors themeColors = Theme.of(context).extension<ThemeColors>()!;

    return ElevatedButton(
      style: ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsetsGeometry?>(
            widget.padding ?? themeElevatedButton.padding),
        foregroundColor: MaterialStateProperty.all<Color?>(
            widget.foregroundColor ?? themeColors.textColor),
        backgroundColor: MaterialStateProperty.all<Color?>(
            widget.backgroundColor ?? themeColors.fillColor),
        textStyle: MaterialStateProperty.all<TextStyle?>(
            themeElevatedButton.textStyle),
        shape: MaterialStateProperty.all<RoundedRectangleBorder?>(
            themeElevatedButton.shape),
        elevation: MaterialStateProperty.all(widget.elevation ?? 0),
      ),
      onPressed: widget.onPressed,
      child: Text(widget.textButton, overflow: TextOverflow.ellipsis),
    );
  }
}
