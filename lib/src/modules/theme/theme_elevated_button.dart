import 'package:flutter/material.dart';

import 'package:game_tracker/src/modules/theme/constants.dart';

class ThemeElevatedButton extends ThemeExtension<ThemeElevatedButton> {
  final EdgeInsetsGeometry? padding;
  final Color? foregroundColor;
  final Color? backgroundColor;
  final TextStyle? textStyle;
  final RoundedRectangleBorder? shape;

  ThemeElevatedButton({
    this.padding = const EdgeInsets.symmetric(
        horizontal: Constants.horizontalButtonPadding,
        vertical: Constants.verticalButtonPadding),
    this.foregroundColor = Constants.textColorLight,
    this.backgroundColor = Constants.primaryColor,
    this.textStyle = const TextStyle(fontWeight: FontWeight.bold),
    this.shape = const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(Constants.borderRadius)),
    ),
  });

  @override
  ThemeExtension<ThemeElevatedButton> copyWith({
    EdgeInsetsGeometry? padding,
    Color? foregroundColor,
    Color? backgroundColor,
    TextStyle? textStyle,
    RoundedRectangleBorder? shape,
  }) {
    return ThemeElevatedButton(
      padding: padding ?? this.padding,
      foregroundColor: foregroundColor ?? this.foregroundColor,
      backgroundColor: backgroundColor ?? this.backgroundColor,
      textStyle: textStyle ?? this.textStyle,
      shape: shape ?? this.shape,
    );
  }

  @override
  ThemeExtension<ThemeElevatedButton> lerp(
      ThemeExtension<ThemeElevatedButton>? other, double t) {
    if (other is! ThemeElevatedButton) {
      return this;
    }
    return ThemeElevatedButton(
      foregroundColor: Color.lerp(foregroundColor, other.foregroundColor, t),
      backgroundColor: Color.lerp(backgroundColor, other.backgroundColor, t),
    );
  }
}
