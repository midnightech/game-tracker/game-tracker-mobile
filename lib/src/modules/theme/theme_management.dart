import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:get/get.dart';

class ThemeManagement extends GetxController {
  RxBool isDark = false.obs;

  void getThemeSystem() {
    Brightness brightness = SchedulerBinding.instance.window.platformBrightness;
    toggleTheme(brightness == Brightness.dark);
  }

  void toggleTheme(bool value) {
    isDark.value = value ? true : false;
  }
}
