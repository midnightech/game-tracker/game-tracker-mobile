import 'package:flutter/material.dart';

class ThemeColors extends ThemeExtension<ThemeColors> {
  final Color? fillColor;
  final Color? textColor;

  ThemeColors({
    this.fillColor,
    this.textColor,
  });

  @override
  ThemeExtension<ThemeColors> copyWith({
    Color? fillColor,
    Color? textColor,
  }) {
    return ThemeColors(
      fillColor: fillColor ?? this.fillColor,
      textColor: textColor ?? this.textColor,
    );
  }

  @override
  ThemeExtension<ThemeColors> lerp(
      ThemeExtension<ThemeColors>? other, double t) {
    if (other is! ThemeColors) {
      return this;
    }
    return ThemeColors(
      fillColor: Color.lerp(fillColor, other.fillColor, t),
      textColor: Color.lerp(textColor, other.textColor, t),
    );
  }
}
