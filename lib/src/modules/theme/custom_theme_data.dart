import 'package:flutter/material.dart';

import 'package:game_tracker/src/modules/theme/constants.dart';
import 'package:game_tracker/src/modules/theme/theme_colors.dart';
import 'package:game_tracker/src/modules/theme/theme_elevated_button.dart';
import 'package:game_tracker/src/modules/theme/theme_text_form_field.dart';

class CustomThemeData {
  ThemeData themeDataLight = ThemeData(
    useMaterial3: true,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsetsGeometry?>(
          const EdgeInsets.symmetric(
              horizontal: Constants.horizontalButtonPadding,
              vertical: Constants.verticalButtonPadding),
        ),
        foregroundColor:
            MaterialStateProperty.all<Color?>(Constants.textColorLight),
        backgroundColor:
            MaterialStateProperty.all<Color?>(Constants.primaryColor),
        textStyle: MaterialStateProperty.all<TextStyle?>(
          const TextStyle(fontWeight: FontWeight.bold),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder?>(
          const RoundedRectangleBorder(
            borderRadius:
                BorderRadius.all(Radius.circular(Constants.borderRadius)),
          ),
        ),
      ),
    ),
    colorScheme: ColorScheme.fromSeed(
        seedColor: Constants.primaryColor, brightness: Brightness.light),
    switchTheme: SwitchThemeData(
      trackColor: MaterialStateProperty.all<Color>(Constants.fillColorLight),
      thumbColor: MaterialStateProperty.all<Color>(Constants.primaryColor),
    ),
    extensions: <ThemeExtension<dynamic>>[
      ThemeColors(
        fillColor: Constants.fillColorLight,
        textColor: Constants.textColorLight,
      ),
      ThemeTextFormField(fillColor: Constants.fillColorLight),
      ThemeElevatedButton(),
    ],
  );

  ThemeData themeDataDark = ThemeData(
    useMaterial3: true,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsetsGeometry?>(
          const EdgeInsets.symmetric(
              horizontal: Constants.horizontalButtonPadding,
              vertical: Constants.verticalButtonPadding),
        ),
        foregroundColor:
            MaterialStateProperty.all<Color?>(Constants.textColorLight),
        backgroundColor:
            MaterialStateProperty.all<Color?>(Constants.primaryColor),
        textStyle: MaterialStateProperty.all<TextStyle?>(
          const TextStyle(fontWeight: FontWeight.bold),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder?>(
          const RoundedRectangleBorder(
            borderRadius:
                BorderRadius.all(Radius.circular(Constants.borderRadius)),
          ),
        ),
      ),
    ),
    colorScheme: ColorScheme.fromSeed(
            seedColor: Constants.primaryColor, brightness: Brightness.dark)
        .copyWith(brightness: Brightness.dark),
    switchTheme: SwitchThemeData(
      trackColor: MaterialStateProperty.all<Color>(Constants.fillColorDark),
      thumbColor: MaterialStateProperty.all<Color>(Constants.primaryColor),
    ),
    extensions: <ThemeExtension<dynamic>>[
      ThemeColors(
        fillColor: Constants.fillColorDark,
        textColor: Constants.textColorDark,
      ),
      ThemeTextFormField(fillColor: Constants.fillColorDark),
      ThemeElevatedButton(),
    ],
  );
}
