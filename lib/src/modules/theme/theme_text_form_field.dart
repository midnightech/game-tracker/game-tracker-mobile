import 'package:flutter/material.dart';

import 'package:game_tracker/src/modules/theme/constants.dart';

class ThemeTextFormField extends ThemeExtension<ThemeTextFormField> {
  final OutlineInputBorder? outlineInputBorder;
  final Color? fillColor;
  final TextStyle? errorStyle;

  ThemeTextFormField({
    this.outlineInputBorder = const OutlineInputBorder(
      borderSide: BorderSide(
        width: 0,
        style: BorderStyle.none,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(Constants.borderRadius),
      ),
    ),
    this.fillColor = Constants.fillColorLight,
    this.errorStyle = const TextStyle(
      fontWeight: FontWeight.w500,
    ),
  });

  @override
  ThemeTextFormField copyWith({
    OutlineInputBorder? outlineInputBorder,
    Color? fillColor,
  }) {
    return ThemeTextFormField(
      outlineInputBorder: outlineInputBorder ?? this.outlineInputBorder,
      fillColor: fillColor ?? this.fillColor,
    );
  }

  @override
  ThemeTextFormField lerp(
    ThemeExtension<ThemeTextFormField>? other,
    double t,
  ) {
    if (other is! ThemeTextFormField) {
      return this;
    }
    return ThemeTextFormField(
      fillColor: Color.lerp(fillColor, other.fillColor, t),
    );
  }
}
