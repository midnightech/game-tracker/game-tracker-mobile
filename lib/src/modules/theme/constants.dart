import 'package:flutter/material.dart';

class Constants {
  static const Color primaryColor = Color(0xFFFFEE58);
  static const Color textColorLight = Color(0x99000000);
  static const Color textColorDark = Color(0x99FFFFFF);

  static const Color fillColorLight = Color(0xFFEEEEEE);
  static const Color hoverColorLight = Color(0x99DDDDDD);
  static const Color fillColorDark = Color(0xFF404040);
  static const Color hoverColorDark = Color(0x99505050);

  static const double borderRadius = 10;

  static const double pagePadding = 8;
  static const double verticalButtonPadding = 28;
  static const double horizontalButtonPadding = 12;
  static const double buttonPadding = 8;
}
