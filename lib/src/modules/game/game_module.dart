import 'package:flutter_modular/flutter_modular.dart';

import 'presenter/pages/discovery.dart';
import 'presenter/pages/game_info.dart';

class GameModule extends Module {
  @override
  List<Bind> get binds => [];
  @override
  List<ModularRoute> get routes => [
        ChildRoute('/', child: (context, args) => const DiscoveryPage()),
        ChildRoute('/info', child: (context, args) => const GameInfoPage()),
      ];
}
