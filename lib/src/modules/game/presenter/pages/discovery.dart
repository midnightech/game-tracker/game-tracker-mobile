import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class DiscoveryPage extends StatelessWidget {
  const DiscoveryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Discovery')),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: () {
                Modular.to.pushNamed('./info');
              },
              child: const Text('Game Info'),
            ),
            ElevatedButton(
              onPressed: () {
                Modular.to.pushNamed('/profile/');
              },
              child: const Text('Profile'),
            ),
          ],
        ),
      ),
    );
  }
}
