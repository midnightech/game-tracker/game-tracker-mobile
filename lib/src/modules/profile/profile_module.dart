import 'package:flutter_modular/flutter_modular.dart';

import 'presenter/pages/profile.dart';
import 'presenter/pages/user_list.dart';

class ProfileModule extends Module {
  @override
  List<Bind> get binds => [];
  @override
  List<ModularRoute> get routes => [
        ChildRoute('/', child: (context, args) => const ProfilePage()),
        ChildRoute('/list', child: (context, args) => const UserListPage()),
      ];
}
