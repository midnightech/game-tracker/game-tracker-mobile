import 'package:flutter_modular/flutter_modular.dart';

import 'package:game_tracker/src/modules/theme/custom_theme_data.dart';
import 'package:game_tracker/src/modules/theme/theme_management.dart';
import 'package:game_tracker/src/services/i18n/main.dart';
import 'package:game_tracker/src/services/state_manager/state_manager.dart';

import 'auth/presenter/auth_module.dart';
import 'game/game_module.dart';
import 'profile/profile_module.dart';
import 'splash_screen.dart';

class AppModule extends Module {
  final TransitionType transitionDefault = TransitionType.leftToRightWithFade;
  final Duration durationDefault = const Duration(milliseconds: 800);

  @override
  List<Bind> get binds => [
        Bind.lazySingleton((i) => ThemeManagement()),
        Bind.lazySingleton((i) => CustomThemeData()),
        Bind.lazySingleton((i) => StateManager()),
        Bind.lazySingleton((i) => Internacionalization()),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/', child: (context, args) => const SplashScreen()),
        ModuleRoute(
          '/auth',
          module: AuthModule(),
          transition: transitionDefault,
          duration: durationDefault,
        ),
        ModuleRoute(
          '/game',
          module: GameModule(),
          transition: transitionDefault,
          duration: durationDefault,
        ),
        ModuleRoute(
          '/profile',
          module: ProfileModule(),
          transition: transitionDefault,
          duration: durationDefault,
        ),
      ];
}
