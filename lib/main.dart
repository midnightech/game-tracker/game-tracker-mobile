import 'package:flutter/material.dart';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:game_tracker/src/modules/app_module.dart';
import 'package:game_tracker/src/modules/app_widget.dart';

void main() {
  Modular.to.addListener(() {
    print(Modular.to.path);
  });

  return runApp(ModularApp(
    module: AppModule(),
    child: const AppWidget(),
  ));
}
